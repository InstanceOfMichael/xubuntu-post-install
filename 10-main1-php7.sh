#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

set -e
set -x #echo on

echo "SUDO_USER: $SUDO_USER"

apt-get install -y python-software-properties
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y php7.0
apt-get install -y \
    php-pear php7.0-gd php7.0-dev \
    imagemagick php7.0-imagick \
    php7.0-cli php7.0-dev  php7.0-fpm \
    php7.0-bcmath php7.0-mbstring php7.0-imap php7.0-soap \
    php7.0-mysql php7.0-pgsql php7.0-sqlite3 
pear channel-discover pear.phpmd.org
pear channel-discover pear.pdepend.org
pear install --alldeps phpmd/PHP_PMD

wget http://cs.sensiolabs.org/get/php-cs-fixer.phar -O /usr/local/bin/php-cs-fixer
chmod a+x /usr/local/bin/php-cs-fixer

apt-get install beanstalkd -y

apt-get install -y \
    mysql-server \
    php7.0-mysql php7.0-mysqlnd php7.0-mysqlnd
sudo -H -u $SUDO_USER -s bash -c "mysql_install_db" /usr/bin/mysql_secure_installation

# splitting here because this is a likely fail point
