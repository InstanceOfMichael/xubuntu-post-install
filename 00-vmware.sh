#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

set -e
set -x #echo on

# http://www.liberiangeek.net/2015/06/installing-vmware-guest-tools-in-ubuntu-15-04/
apt-get update -y
apt-get dist-upgrade -y
apt-get autoremove
apt-get install open-vm-tools open-vm-tools-dkms
sudo -H -u $SUDO_USER -s bash -c "tar -xvf /media/$SUDO_USER/\"VMware Tools\"/VMwareTools*.gz -C /tmp"
/tmp/vmware-tools-distrib/vmware-install.pl -d
