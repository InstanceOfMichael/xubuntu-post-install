#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

set -e
set -x #echo on

echo "SUDO_USER: $SUDO_USER"

apt-get remove apache2 -y

# echo "install Postgre 9.5+ http://www.postgresql.org/download/linux/ubuntu/ "
touch /etc/apt/sources.list.d/pgdg.list
echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
  apt-key add -

apt-get update -y
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get autoremove -y

apt-get install -y curl

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

apt-get install -y \
    p7zip-rar p7zip-full unace unrar zip unzip sharutils rar uudeview mpack arj cabextract file-roller \
    screen \
    git curl \
    nano screen geany geany-plugins meld \
    gcc g++ postgresql-10 \
    silversearcher-ag \
    dnsutils linkchecker \
    chromium-browser \
    nodejs \
    postgresql-10 \
    redis-server \
    nginx \

sudo snap install sublime-text --classic

npm install -g \
    diff-so-fancy \
    nodemon eslint

# https://github.com/so-fancy/diff-so-fancy
cat >~/.gitconfig <<EOF4
[alias]
    dsf = "!f() { [ \"$GIT_PREFIX\" != \"\" ] && cd "$GIT_PREFIX"; git diff --color $@ | diff-so-fancy | less --tabs=4 -RFX; }; f"
EOF4

mkdir ~/.ssh || true

cat >~/.ssh/config <<EOF5
# This reduces your number of "Broken Pipe" error messages
ServerAliveInterval 100
EOF5

chown -R $SUDO_USER:$SUDO_USER ~/.*
chown -R $SUDO_USER:$SUDO_USER ~/*

git config --global pull.rebase true

#curl -sS https://getcomposer.org/installer | php
#mv composer.phar /usr/local/bin/composer

apt-get update -y
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get autoremove -y


# echo "yourhostname" > /etc/hostname
nano /etc/hostname
